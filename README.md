# Running Mykrobe
```
nextflow run mykrobe.nf -with-singularity /docker/singularity/phelimb_mykrobe_predictor-2017-08-04-988bdbb8ee1a.img --bam_dir input_dir --pattern_match=*.bam --output_dir output_dir --species staph
```
